<?php

require_once 'Controller.php';
require_once '../src/FormBmlDoc.php';

class Forms extends Controller
{
    public function form()
    {
        $text_entry_fields = array(
            array(
                'name' => 'Text',
                'mode' => 'text',
                'mandatory' => 'true',
            ),
            array(
                'name' => 'Number',
                'mode' => 'numeric',
                'mandatory' => 'true',
            ),
        );
        $submit_url = $this->gen_in_app_url('demo_list', null, 'Pages');
        $this->bml_doc = new FormBmlDoc(
            TTL, DEV_ID, APP_ID, $_COOKIE, 'test', $text_entry_fields, $submit_url, 'Form',
            LTYPE
        );
        $this->render_bml($this->bml_doc->get_bml());
    }
}
