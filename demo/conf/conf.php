<?php

require_once 'conf_dev.php';
//require_once 'conf_prod.php';

define('APP_NAME', 'Demo App');
define('APP_ID', '1');
define('DEV_ID', '1');

if (empty($_COOKIE['binusys_home_app_url'])) {
    define('HOME_URL', 'http://apps.binu.net/apps/mybinu/index.php');
} else {
    define('HOME_URL', $_COOKIE['binusys_home_app_url']);
}

define('DEFAULT_CONTROLLER', 'Pages');
define('DEFAULT_METHOD', 'home');

define('FONT', 'Arial Unicode MS');

define('HEADING_COLOR', '#444444');
define('BODY_TEXT_COLOR', '#333333');
define('BUTTON_TEXT_COLOR', '#DDDDDD');
define('LINK_TEXT_COLOR', '#05B8CC');
define('FOOTER_BG_COLOR', '#9bcc01');
define('FOOTER_TEXT_COLOR', '#222222');
define('BUTTON_COLOR', '#444444');
define('BINU_GREEN', '#9bcc01');
