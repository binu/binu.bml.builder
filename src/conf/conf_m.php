<?php

return array(
    'font_size' => 15,
    'line_height' => 19,
    'title_font_size' => 16,
    'title_line_height' => 18,
    'toolbar_height' => 25,
    'indent' => 7,
    'screen_size' => 'm',
);
