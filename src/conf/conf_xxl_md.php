<?php

return array(
    'font_size' => 40,
    'line_height' => 60,
    'title_font_size' => 45,
    'title_line_height' => 60,
    'toolbar_height' => 90,
    'indent' => 30,
    'screen_size' => 'xxl_md',
);
