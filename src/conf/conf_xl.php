<?php

return array(
    'font_size' => 22,
    'line_height' => 35,
    'title_font_size' => 26,
    'title_line_height' => 40,
    'toolbar_height' => 60,
    'indent' => 17,
    'screen_size' => 'xl',
);
