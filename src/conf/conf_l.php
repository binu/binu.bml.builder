<?php

return array(
    'font_size' => 17,
    'line_height' => 22,
    'title_font_size' => 19,
    'title_line_height' => 25,
    'toolbar_height' => 35,
    'indent' => 10,
    'screen_size' => 'l',
);
