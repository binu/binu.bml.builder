<?php

return array(
    'font_size' => 58,
    'line_height' => 90,
    'title_font_size' => 60,
    'title_line_height' => 90,
    'toolbar_height' => 100,
    'indent' => 40,
    'screen_size' => 'xxl_hd',
);
