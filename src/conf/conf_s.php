<?php

return array(
    'font_size' => 13,
    'line_height' => 17,
    'title_font_size' => 13,
    'title_line_height' => 17,
    'toolbar_height' => 20,
    'indent' => 2,
    'screen_size' => 's',
);
